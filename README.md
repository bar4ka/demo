# Github Demo

In this demo implementation focus was:

1. Using MVVMP (Model, View, ViewModel, Presenter) design pattern with Android Binding
1. Using Dagger 2
1. Using [square/flow](https://github.com/square/flow)
1. Unit Testing with [Spek](http://spekframework.org/)

Due to time limitations, I didn't implement full specs for classes, but there should be enough implemented to show case how Unit Tests would be implemented.
