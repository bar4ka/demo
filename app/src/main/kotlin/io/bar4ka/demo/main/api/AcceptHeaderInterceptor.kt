package io.bar4ka.demo.main.api

import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AcceptHeaderInterceptor
@Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
            .newBuilder()
            .addHeader("Accept", "application/json")
            .build()

        return chain.proceed(request)
    }
}
