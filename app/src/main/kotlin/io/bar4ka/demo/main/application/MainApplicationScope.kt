package io.bar4ka.demo.main.application

import javax.inject.Scope

@Scope
annotation class MainApplicationScope
