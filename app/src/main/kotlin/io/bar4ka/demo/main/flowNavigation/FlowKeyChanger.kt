package io.bar4ka.demo.main.flowNavigation

import android.app.Activity
import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding.view.attaches
import com.jakewharton.rxbinding.view.detaches
import io.bar4ka.demo.BR
import io.bar4ka.demo.core.KeyComponentGetter
import io.bar4ka.demo.core.mvvmp.Presenter
import io.bar4ka.demo.core.navigation.Key
import flow.Direction
import flow.KeyChanger
import flow.State
import flow.TraversalCallback

class FlowKeyChanger(
    private val activity: Activity,
    private val keyComponentGetter: KeyComponentGetter
) : KeyChanger {

    override fun changeKey(outgoingState: State?, incomingState: State, direction: Direction, incomingContexts: MutableMap<Any, Context>, callback: TraversalCallback) {
        val key = incomingState.getKey<Key>()
        val context = incomingContexts[key]!!
        val component = keyComponentGetter.getComponent(key)
        val presenter = component.presenter
        val viewModel = component.viewModel
        val outgoingView: View?

        if (outgoingState != null) {
            val view = activity.findViewById<ViewGroup>(android.R.id.content)
            outgoingView = view.getChildAt(0)
            outgoingState.save(outgoingView)
        }

        presenter.dispatchCreated()

        val view = inflateLayout(context, viewModel.layoutId)
        DataBindingUtil.bind<ViewDataBinding>(view)!!.setVariable(BR.viewModel, viewModel)

        incomingState.restore(view)

        observeViewAttach(presenter, view)

        activity.setContentView(view)

        callback.onTraversalCompleted()
    }

    private fun inflateLayout(context: Context, @LayoutRes layout: Int): View {
        val inflater = LayoutInflater.from(context)
        return inflater.inflate(layout, null)
    }

    private fun observeViewAttach(presenter: Presenter<View>, view: View) {
        view.attaches()
            .take(1)
            .subscribe { presenter.dispatchAttachView(view) }

        view.detaches()
            .take(1)
            .subscribe { presenter.dispatchDetachView(view) }
    }
}
