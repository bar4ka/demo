package io.bar4ka.demo.main.storage

import io.bar4ka.demo.main.application.MainApplicationScope
import dagger.Module
import dagger.Provides
import io.realm.Realm

@Module
class StorageModule {

    @Provides
    @MainApplicationScope
    fun provideRealmConfiguration() = Realm.getDefaultConfiguration()!!

}
