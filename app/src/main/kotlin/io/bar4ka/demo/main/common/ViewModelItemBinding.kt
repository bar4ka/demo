package io.bar4ka.demo.main.common

import io.bar4ka.demo.BR
import io.bar4ka.demo.core.mvvmp.ViewModel
import me.tatarka.bindingcollectionadapter2.ItemBinding
import me.tatarka.bindingcollectionadapter2.OnItemBind

class ViewModelItemBinding<VM>(
    private val onIndexBound: (Int) -> Unit
) : OnItemBind<VM> where VM : ViewModel {

    override fun onItemBind(itemBinding: ItemBinding<*>, position: Int, item: VM) {
        itemBinding.set(BR.viewModel, item.layoutId)
        onIndexBound(position)
    }

}
