package io.bar4ka.demo.main.activity

import io.bar4ka.demo.core.navigation.DEFAULT_KEY
import io.bar4ka.demo.core.navigation.Key
import io.bar4ka.demo.core.navigation.Navigation
import io.bar4ka.demo.main.flowNavigation.FlowNavigation
import io.bar4ka.demo.repositories.RepositoriesKey
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class MainActivityModule {
    companion object {
        val DEFAULT_USERNAME = "JakeWharton"
    }

    @Provides
    @MainActivityScope
    fun provideNavigation(flowNavigation: FlowNavigation): Navigation = flowNavigation

    @Provides
    @Named(DEFAULT_KEY)
    //FIXME: Replace
    fun provideDefaultKey(): Key = RepositoriesKey(DEFAULT_USERNAME)
}
