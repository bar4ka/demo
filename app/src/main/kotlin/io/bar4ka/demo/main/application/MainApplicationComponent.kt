package io.bar4ka.demo.main.application

import android.app.Application
import io.bar4ka.demo.main.activity.MainActivityComponent
import io.bar4ka.demo.main.api.ApiModule
import io.bar4ka.demo.main.storage.StorageModule
import dagger.BindsInstance
import dagger.Component

@MainApplicationScope
@Component(modules = [
    MainApplicationModule::class,
    ApiModule::class,
    StorageModule::class
])
interface MainApplicationComponent {

    val mainActivityComponent: MainActivityComponent

    @Component.Builder
    interface Builder {

        fun build(): MainApplicationComponent

        @BindsInstance
        fun application(application: Application): Builder
    }
}
