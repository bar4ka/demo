package io.bar4ka.demo.main.application

import android.app.Application
import io.bar4ka.demo.BuildConfig
import io.realm.Realm
import timber.log.Timber

class MainApplication : Application() {

    val component by lazy {
        DaggerMainApplicationComponent.builder()
            .application(this)
            .build()
    }

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        Realm.init(this)
    }
}
