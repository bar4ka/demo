package io.bar4ka.demo.main.api

import retrofit2.Retrofit

inline fun <reified T> Retrofit.create() = create(T::class.java)!!
