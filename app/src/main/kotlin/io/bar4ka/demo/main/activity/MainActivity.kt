package io.bar4ka.demo.main.activity

import android.app.Activity
import android.content.Context
import io.bar4ka.demo.core.navigation.Navigation
import io.bar4ka.demo.main.application.MainApplication
import javax.inject.Inject

class MainActivity : Activity() {

    @Inject
    internal lateinit var navigation: Navigation

    override fun attachBaseContext(baseContext: Context) {
        val component = (baseContext.applicationContext as MainApplication).component.mainActivityComponent
        component.inject(this)

        val navigationContext = navigation.setup(baseContext, this)
        super.attachBaseContext(navigationContext)
    }

    override fun onBackPressed() {
        if (!navigation.goBack(this)) {
            super.onBackPressed()
        }
    }
}
