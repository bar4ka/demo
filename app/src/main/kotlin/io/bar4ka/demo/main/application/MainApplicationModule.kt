package io.bar4ka.demo.main.application

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class MainApplicationModule {

    @Provides
    @MainApplicationScope
    fun provideContext(application: Application): Context = application
}
