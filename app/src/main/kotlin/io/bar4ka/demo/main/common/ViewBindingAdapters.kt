package io.bar4ka.demo.main.common

import android.databinding.BindingAdapter
import android.view.View
import com.jakewharton.rxbinding.view.RxView
import rx.subjects.Subject

object ViewBindingAdapters {

    @JvmStatic
    @BindingAdapter("onClicked")
    fun bindOnClicked(view: View, onClicked: Subject<Void, Void>) {
        RxView.clicks(view)
            .subscribe(onClicked)
    }
}
