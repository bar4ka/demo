package io.bar4ka.demo.main.activity

import io.bar4ka.demo.core.KeyComponentGetter
import io.bar4ka.demo.repositories.RepositoriesBindingModule
import dagger.Subcomponent
import dagger.android.AndroidInjector

@MainActivityScope
@Subcomponent(modules = [
    MainActivityModule::class,
    RepositoriesBindingModule::class
])
interface MainActivityComponent : AndroidInjector<MainActivity> {

    val keyComponentGetter: KeyComponentGetter
}
