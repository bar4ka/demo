package io.bar4ka.demo.main.flowNavigation

import android.app.Activity
import android.content.Context
import io.bar4ka.demo.core.KeyComponentGetter
import io.bar4ka.demo.core.navigation.DEFAULT_KEY
import io.bar4ka.demo.core.navigation.Key
import io.bar4ka.demo.core.navigation.Navigation
import flow.Flow
import flow.KeyDispatcher
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Provider

class FlowNavigation
@Inject constructor(
    @Named(DEFAULT_KEY) private val defaultKeyProvider: Provider<Key>,
    private val keyComponentGetter: KeyComponentGetter
) : Navigation {

    private lateinit var changer: FlowKeyChanger

    override fun setup(baseContext: Context, activity: Activity): Context {
        changer = FlowKeyChanger(activity, keyComponentGetter)

        //TODO: Register `ServicesFactory` to maintain Key's state properly
        return Flow.configure(baseContext, activity)
            .defaultKey(defaultKeyProvider.get())
            .dispatcher(KeyDispatcher.configure(activity, changer).build())
            .install()
    }

    override fun <K : Key> getKey(context: Context) = Flow.getKey<K>(context)!!

    override fun navigate(context: Context, key: Key) = Flow.get(context).set(key)

    override fun goBack(context: Context) = Flow.get(context).goBack()
}
