package io.bar4ka.demo.main.api

import com.google.gson.FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.bar4ka.demo.BuildConfig
import io.bar4ka.demo.main.application.MainApplicationScope
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import okhttp3.logging.HttpLoggingInterceptor.Logger
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

@Module
class ApiModule {

    @Provides
    @MainApplicationScope
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        gson: Gson
    ): Retrofit {

        return Retrofit.Builder()
            .baseUrl("https://api.github.com/")
            .addCallAdapterFactory(RxJavaCallAdapterFactory.createAsync())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .build()
    }

    @Provides
    @MainApplicationScope
    fun provideOkhttpClient(
        acceptHeaderInterceptor: AcceptHeaderInterceptor,
        loggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient {

        val builder = OkHttpClient.Builder()
            .addInterceptor(acceptHeaderInterceptor)

        if (BuildConfig.DEBUG) {
            builder.addInterceptor(loggingInterceptor)
        }

        return builder.build()
    }

    @Provides
    @MainApplicationScope
    fun provideLoggingInterceptor(logger: Logger) = HttpLoggingInterceptor(logger).apply {
        level = BODY
    }

    @Provides
    @MainApplicationScope
    fun provideLogger() = Logger {
        Timber.d(it)
    }

    @Provides
    @MainApplicationScope
    fun provideGson(): Gson {
        return GsonBuilder()
            .setFieldNamingPolicy(LOWER_CASE_WITH_UNDERSCORES)
            .create()
    }
}
