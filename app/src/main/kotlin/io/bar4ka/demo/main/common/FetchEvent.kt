package io.bar4ka.demo.main.common

import rx.Observable

class FetchEvent<out P, T>(
    val fetchParameters: P,
    val transformer: Observable.Transformer<T, T>
)
