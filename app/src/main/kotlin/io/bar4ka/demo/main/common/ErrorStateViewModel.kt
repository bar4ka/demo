package io.bar4ka.demo.main.common

import android.databinding.ObservableField
import io.bar4ka.demo.R
import io.bar4ka.demo.core.mvvmp.ViewModel
import rx.subjects.PublishSubject
import javax.inject.Inject

class ErrorStateViewModel
@Inject constructor() : ViewModel {
    val message = ObservableField<String>()
    val onRetry = PublishSubject.create<Void>()!!

    override val layoutId = R.layout.error_screen_state
}
