package io.bar4ka.demo.main.activity

import javax.inject.Scope

@Scope
annotation class MainActivityScope
