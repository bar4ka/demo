package io.bar4ka.demo.main.common

enum class ScreenState {
    Content, Loading, Error
}
