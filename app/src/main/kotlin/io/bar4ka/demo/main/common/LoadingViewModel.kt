package io.bar4ka.demo.main.common

import android.databinding.ObservableBoolean
import io.bar4ka.demo.R
import io.bar4ka.demo.core.mvvmp.ViewModel
import javax.inject.Inject

class LoadingViewModel
@Inject constructor() : ViewModel {

    override val layoutId = R.layout.loading_screen_state

    val isVisible = ObservableBoolean()

}
