package io.bar4ka.demo.core

interface KeyComponentFactory<out C : KeyComponent<*, *>> {
    fun create(): C
}
