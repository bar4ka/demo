package io.bar4ka.demo.core.mvvmp

interface Mapper<in S, in D> {

    fun map(source: S, destination: D)
}
