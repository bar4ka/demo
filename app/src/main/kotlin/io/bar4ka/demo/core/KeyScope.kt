package io.bar4ka.demo.core

import javax.inject.Scope
import kotlin.reflect.KClass

@Scope
annotation class KeyScope(val kClass: KClass<*>)
