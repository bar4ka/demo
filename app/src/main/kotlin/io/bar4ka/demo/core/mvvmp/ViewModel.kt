package io.bar4ka.demo.core.mvvmp

interface ViewModel {
    val layoutId: Int
}
