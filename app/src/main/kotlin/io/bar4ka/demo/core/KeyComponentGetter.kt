package io.bar4ka.demo.core

import io.bar4ka.demo.core.navigation.Key
import javax.inject.Inject

class KeyComponentGetter
@Inject constructor(
    private val components: Map<Class<*>, @JvmSuppressWildcards KeyComponentFactory<*>>
) {

    fun getComponent(key: Key): KeyComponent<*, *> {
        val componentFactory = components[key::class.java]

        return requireNotNull(componentFactory) { "Component is not bound for Key: $key" }
            .create()
    }
}
