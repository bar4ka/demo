package io.bar4ka.demo.core.mvvmp

import android.view.View

abstract class ViewModelPresenter<out VM : ViewModel> : Presenter<View>() {

    abstract val viewModel: VM
}
