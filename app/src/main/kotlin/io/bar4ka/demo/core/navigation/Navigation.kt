package io.bar4ka.demo.core.navigation

import android.app.Activity
import android.content.Context

interface Navigation {

    fun setup(baseContext: Context, activity: Activity): Context

    fun <K : Key> getKey(context: Context): K

    fun navigate(context: Context, key: Key)

    fun goBack(context: Context): Boolean
}
