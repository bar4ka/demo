package io.bar4ka.demo.core.mvvmp

@Suppress("unused")
sealed class Lifecycle<out V>

class Created<out V> : Lifecycle<V>()

class AttachView<out V>(val view: V) : Lifecycle<V>()

class DetachView<out V>(val view: V) : Lifecycle<V>()

class Destroyed<out V> : Lifecycle<V>()
