package io.bar4ka.demo.core

import io.bar4ka.demo.core.mvvmp.ViewModel
import io.bar4ka.demo.core.mvvmp.ViewModelPresenter

interface KeyComponent<out P : ViewModelPresenter<VM>, out VM : ViewModel> {
    val presenter: P
    val viewModel: VM
}
