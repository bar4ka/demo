package io.bar4ka.demo.core.mvvmp

import rx.Observable
import rx.subjects.ReplaySubject

abstract class Presenter<V> {

    private val lifecycle = ReplaySubject.createWithSize<Lifecycle<V>>(1)

    fun dispatchLifecycle(events: Observable<Lifecycle<V>>) {
        events.subscribe(lifecycle)
    }

    fun dispatchCreated() = lifecycle.onNext(Created())

    fun dispatchAttachView(view: V) = lifecycle.onNext(AttachView(view))

    fun dispatchDetachView(view: V) = lifecycle.onNext(DetachView(view))

    fun dispatchDestroyed() = lifecycle.onNext(Destroyed())

    fun lifecycle() = lifecycle.asObservable()!!

    fun created(): Observable<Created<V>> = lifecycleEvent()

    fun attachView(): Observable<AttachView<V>> = lifecycleEvent()

    fun detachView(): Observable<DetachView<V>> = lifecycleEvent()

    fun destroyed(): Observable<Destroyed<V>> = lifecycleEvent()

    private inline fun <reified T : Lifecycle<V>> lifecycleEvent() =
        lifecycle
            .filter { it is T }
            .map { it as T }!!
}
