package io.bar4ka.demo.repositories.mappers

import io.bar4ka.demo.core.KeyScope
import io.bar4ka.demo.core.mvvmp.Mapper
import io.bar4ka.demo.repositories.RepositoriesKey
import io.bar4ka.demo.repositories.models.Repository
import io.bar4ka.demo.repositories.viewModels.RepositoryViewModel
import javax.inject.Inject

@KeyScope(RepositoriesKey::class)
class RepositoryMapper
@Inject constructor() : Mapper<Repository, RepositoryViewModel> {

    override fun map(source: Repository, destination: RepositoryViewModel) {
        with(destination) {
            title.set(source.fullName)
            description.set(source.description)
            language.set(source.language)
            //FIXME: Format
            stargazersCount.set(source.stargazersCount.toString())
            updatedAt.set(source.pushedAt.toString())
            license.set(source.license?.name)
        }
    }
}
