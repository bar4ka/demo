package io.bar4ka.demo.repositories

import io.bar4ka.demo.core.KeyComponent
import io.bar4ka.demo.core.KeyComponentFactory
import io.bar4ka.demo.core.KeyScope
import io.bar4ka.demo.repositories.viewModels.RepositoriesViewModel
import dagger.Subcomponent

@KeyScope(RepositoriesKey::class)
@Subcomponent(modules = [RepositoriesModule::class])
interface RepositoriesComponent : KeyComponent<RepositoriesPresenter, RepositoriesViewModel> {

    @Subcomponent.Builder
    interface Builder : KeyComponentFactory<RepositoriesComponent>
}
