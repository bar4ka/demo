package io.bar4ka.demo.repositories

import io.bar4ka.demo.core.KeyScope
import io.bar4ka.demo.repositories.models.RepositoriesResponse
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import javax.inject.Inject

@KeyScope(RepositoriesKey::class)
open class RepositoriesService
@Inject constructor(
    private val api: RepositoriesApi,
    private val storage: RepositoriesStorage
) {

    //FIXME: Replace pagination to be handled by pagination headers received in requests
    // Also use the flags to detect if there are additional pages
    open fun fetch(params: RepositoriesFetchParams): Observable<RepositoriesResponse> {
        return if (params.page == 1) fetchAndCacheFirstPage(params)
        else fetchFromNetwork(params, false)
    }

    private fun fetchAndCacheFirstPage(params: RepositoriesFetchParams): Observable<RepositoriesResponse> {
        return fetchFromStorage(params)
            .onErrorResumeNext(Observable.empty())
            .switchMap {
                Observable.mergeDelayError(
                    Observable.just(it), fetchFromNetwork(params, true)
                )
            }
            .switchIfEmpty(fetchFromNetwork(params, true))
    }

    private fun fetchFromNetwork(params: RepositoriesFetchParams, cacheResults: Boolean) =
        api.getRepositories(params.username, params.page, params.limit)
            .doOnNext {
                if (cacheResults) {
                    storage.deleteAllRepositories()
                    storage.saveRepositories(it)
                }
            }
            .observeOn(AndroidSchedulers.mainThread())!!

    private fun fetchFromStorage(params: RepositoriesFetchParams) =
        storage.loadRepositories(params)
            .filter { it.isNotEmpty() }
}
