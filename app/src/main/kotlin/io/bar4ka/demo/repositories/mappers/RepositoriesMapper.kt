package io.bar4ka.demo.repositories.mappers

import io.bar4ka.demo.core.KeyScope
import io.bar4ka.demo.core.mvvmp.Mapper
import io.bar4ka.demo.repositories.RepositoriesKey
import io.bar4ka.demo.repositories.models.RepositoriesResponse
import io.bar4ka.demo.repositories.viewModels.RepositoriesViewModel
import javax.inject.Inject

@KeyScope(RepositoriesKey::class)
open class RepositoriesMapper
@Inject constructor(
    private val itemMapper: RepositoryMapper
) : Mapper<RepositoriesResponse, RepositoriesViewModel> {

    override fun map(source: RepositoriesResponse, destination: RepositoriesViewModel) {
        with(destination) {
            source.forEach { item ->
                val viewModelItem = newItem(item.id!!)
                    .also { itemMapper.map(item, it) }

                items.add(viewModelItem)
            }

            hasMorePages = source.isNotEmpty()
        }
    }
}
