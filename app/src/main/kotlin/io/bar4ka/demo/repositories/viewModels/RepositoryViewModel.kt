package io.bar4ka.demo.repositories.viewModels

import android.databinding.ObservableField
import io.bar4ka.demo.R
import io.bar4ka.demo.core.mvvmp.ViewModel
import rx.subjects.PublishSubject

class RepositoryViewModel(val id: Int) : ViewModel {

    val title = ObservableField<String>()
    val description = ObservableField<String?>()
    val language = ObservableField<String?>()
    val stargazersCount = ObservableField<String>()
    val updatedAt = ObservableField<String>()
    val license = ObservableField<String?>()

    val onClicked = PublishSubject.create<Void>()!!

    override val layoutId = R.layout.repository_item

}
