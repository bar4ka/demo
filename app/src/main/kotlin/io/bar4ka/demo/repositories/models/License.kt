package io.bar4ka.demo.repositories.models

import io.realm.RealmModel
import io.realm.annotations.RealmClass

@RealmClass
open class License : RealmModel {
    lateinit var name: String
}
