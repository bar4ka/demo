package io.bar4ka.demo.repositories.models

typealias RepositoriesResponse = List<Repository>
