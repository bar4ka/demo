package io.bar4ka.demo.repositories

import io.bar4ka.demo.core.KeyScope
import io.bar4ka.demo.repositories.models.RepositoriesResponse
import io.bar4ka.demo.repositories.models.Repository
import io.realm.Realm
import io.realm.RealmConfiguration
import rx.Observable
import javax.inject.Inject

@KeyScope(RepositoriesKey::class)
class RealmRepositoriesStorage
@Inject constructor(
    private val realmConfiguration: RealmConfiguration
) : RepositoriesStorage {

    override fun saveRepositories(repositories: RepositoriesResponse) {
        val realm = Realm.getInstance(realmConfiguration)
        realm.beginTransaction()
        realm.copyToRealm(repositories)
        realm.commitTransaction()
        realm.close()
    }

    override fun loadRepositories(params: RepositoriesFetchParams): Observable<RepositoriesResponse> {
        return Observable.fromCallable {
            val realm = Realm.getInstance(realmConfiguration)

            val results = realm.where(Repository::class.java)
                .equalTo("owner.login", params.username)
                .findAll()
                .run { realm.copyFromRealm(this) }

            realm.close()

            results
        }
    }

    override fun deleteAllRepositories() {
        val realm = Realm.getInstance(realmConfiguration)
        realm.beginTransaction()
        realm.delete(Repository::class.java)
        realm.commitTransaction()
        realm.close()
    }

}
