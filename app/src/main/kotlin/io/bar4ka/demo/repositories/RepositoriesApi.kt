package io.bar4ka.demo.repositories

import io.bar4ka.demo.repositories.models.RepositoriesResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import rx.Observable

interface RepositoriesApi {

    @GET("users/{username}/repos")
    fun getRepositories(
        @Path("username") username: String,
        @Query("page") page: Int,
        @Query("per_page") limit: Int
    ): Observable<RepositoriesResponse>
}
