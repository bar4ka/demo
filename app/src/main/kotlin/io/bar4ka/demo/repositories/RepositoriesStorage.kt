package io.bar4ka.demo.repositories

import io.bar4ka.demo.repositories.models.RepositoriesResponse
import rx.Observable

interface RepositoriesStorage {
    fun saveRepositories(repositories: RepositoriesResponse)

    fun loadRepositories(params: RepositoriesFetchParams): Observable<RepositoriesResponse>

    fun deleteAllRepositories()
}
