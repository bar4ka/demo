package io.bar4ka.demo.repositories

import io.bar4ka.demo.core.navigation.Key

data class RepositoriesKey(val username: String) : Key
