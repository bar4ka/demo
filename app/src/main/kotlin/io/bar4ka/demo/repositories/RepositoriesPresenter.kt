package io.bar4ka.demo.repositories

import android.support.design.widget.Snackbar
import android.view.View
import io.bar4ka.demo.core.KeyScope
import io.bar4ka.demo.core.mvvmp.ViewModelPresenter
import io.bar4ka.demo.core.navigation.Navigation
import io.bar4ka.demo.main.common.FetchEvent
import io.bar4ka.demo.main.common.ScreenState.*
import io.bar4ka.demo.repositories.Config.ITEMS_PER_PAGE
import io.bar4ka.demo.repositories.mappers.RepositoriesMapper
import io.bar4ka.demo.repositories.models.RepositoriesResponse
import io.bar4ka.demo.repositories.viewModels.RepositoriesViewModel
import rx.Observable
import timber.log.Timber
import javax.inject.Inject

@KeyScope(RepositoriesKey::class)
class RepositoriesPresenter
@Inject constructor(
    override val viewModel: RepositoriesViewModel,
    private val service: RepositoriesService,
    private val mapper: RepositoriesMapper,
    private val navigation: Navigation
) : ViewModelPresenter<RepositoriesViewModel>() {

    init {
        fetchOrigins()
            .switchMap {
                fetch(it.fetchParameters)
                    .compose(it.transformer)
                    .onErrorResumeNext(Observable.empty())
            }
            .subscribe { mapper.map(it, viewModel) }

        viewModel.onClicked
            .subscribe {
                /*
                    Ex: To navigate to details:
                    val detailsKey = RepositoryDetails(it.repo)
                    navigation.navigate(context, detailsKey)
                 */
            }
    }

    //TODO: Extract common fetch logic
    private fun fetchOrigins() =
        onAttachView()
            .mergeWith(onErrorRetry())
            .map {
                val username = navigation.getKey<RepositoriesKey>(it.view.context).username
                FetchEvent(RepositoriesFetchParams(username, 1, ITEMS_PER_PAGE), initialFetchHandler<RepositoriesResponse>(it.view))
            }
            .mergeWith(onPagination())

    private fun onAttachView() = attachView().take(1)

    private fun onErrorRetry() =
        viewModel.errorState.onRetry
            .flatMap { onAttachView() }

    private fun onPagination() =
        viewModel.onIndexBound
            .filter {
                viewModel.hasMorePages
                    && it >= (viewModel.items.size - 3)
                    && !viewModel.paginationLoading.isVisible.get()
            }
            .flatMap { onAttachView() }
            .map {
                val username = navigation.getKey<RepositoriesKey>(it.view.context).username
                val page = Math.ceil(viewModel.items.size / ITEMS_PER_PAGE.toDouble()).toInt() + 1
                FetchEvent(RepositoriesFetchParams(username, page, ITEMS_PER_PAGE), paginationFetchHandler<RepositoriesResponse>(it.view))
            }

    private fun fetch(params: RepositoriesFetchParams) =
        service.fetch(params)
            .doOnError { Timber.e(it, "Fetching Error for: $params") }

    private fun <T> initialFetchHandler(view: View) = Observable.Transformer<T, T> {
        it.doOnSubscribe { viewModel.screenState.set(Loading) }
            .doOnNext {
                viewModel.items.clear()
                viewModel.screenState.set(Content)
            }
            .doOnError {
                //TODO: Use a proper error message
                viewModel.errorState.message.set(it.message)
                if (viewModel.items.isEmpty()) {
                    viewModel.screenState.set(Error)
                } else {
                    showInScreenError(view, it)
                }
            }
    }

    private fun <T> paginationFetchHandler(view: View) = Observable.Transformer<T, T> {
        it.doOnSubscribe { viewModel.paginationLoading.isVisible.set(true) }
            .doOnError {
                //TODO: Use a proper error message
                showInScreenError(view, it)
            }
            .doOnTerminate { viewModel.paginationLoading.isVisible.set(false) }
    }

    private fun showInScreenError(view: View, throwable: Throwable) =
        Snackbar.make(view, throwable.message!!, Snackbar.LENGTH_LONG).show()
}
