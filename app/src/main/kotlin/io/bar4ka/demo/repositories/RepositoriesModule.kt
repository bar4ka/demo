package io.bar4ka.demo.repositories

import io.bar4ka.demo.core.KeyScope
import io.bar4ka.demo.main.api.create
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class RepositoriesModule {

    @Provides
    @KeyScope(RepositoriesKey::class)
    fun provideApi(retrofit: Retrofit): RepositoriesApi = retrofit.create()

    @Provides
    @KeyScope(RepositoriesKey::class)
    fun provideStorage(realmRepositoriesStorage: RealmRepositoriesStorage): RepositoriesStorage = realmRepositoriesStorage
}
