package io.bar4ka.demo.repositories

import io.bar4ka.demo.core.KeyComponentFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap

@Module(subcomponents = [RepositoriesComponent::class])
abstract class RepositoriesBindingModule {

    @Binds
    @IntoMap
    @ClassKey(RepositoriesKey::class)
    abstract fun provideComponentFactory(builder: RepositoriesComponent.Builder): KeyComponentFactory<*>
}
