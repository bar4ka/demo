package io.bar4ka.demo.repositories

data class RepositoriesFetchParams(
    val username: String,
    val page: Int,
    val limit: Int
)
