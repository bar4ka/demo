package io.bar4ka.demo.repositories.models

import io.realm.RealmModel
import io.realm.annotations.RealmClass
import java.util.*

@RealmClass
open class Repository : RealmModel {
    var id: Int? = null
    lateinit var name: String
    var owner: Owner? = null
    lateinit var fullName: String
    var description: String? = null
    var language: String? = null
    var stargazersCount: Long? = null
    lateinit var pushedAt: Date
    var license: License? = null
}
