package io.bar4ka.demo.repositories.viewModels

import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import io.bar4ka.demo.R
import io.bar4ka.demo.core.KeyScope
import io.bar4ka.demo.core.mvvmp.ViewModel
import io.bar4ka.demo.main.common.ErrorStateViewModel
import io.bar4ka.demo.main.common.LoadingViewModel
import io.bar4ka.demo.main.common.ScreenState
import io.bar4ka.demo.main.common.ViewModelItemBinding
import io.bar4ka.demo.repositories.RepositoriesKey
import rx.subjects.PublishSubject
import javax.inject.Inject

@KeyScope(RepositoriesKey::class)
class RepositoriesViewModel
@Inject constructor(
    val errorState: ErrorStateViewModel,
    val paginationLoading: LoadingViewModel
) : ViewModel {
    var hasMorePages: Boolean = true

    val title = ObservableField<String>()
    val description = ObservableField<String>()

    val items = ObservableArrayList<RepositoryViewModel>()
    val itemBinding = ViewModelItemBinding<RepositoryViewModel> {
        onIndexBound.onNext(it)
    }

    val onIndexBound = PublishSubject.create<Int>()!!

    val onClicked = PublishSubject.create<RepositoryViewModel>()!!

    val screenState = ObservableField<ScreenState>(ScreenState.Content)

    fun newItem(id: Int) =
        RepositoryViewModel(id).apply {
            onClicked.map { this }
                .subscribe(this@RepositoriesViewModel.onClicked)
        }

    override val layoutId = R.layout.repositories_view
}
