package io.bar4ka.demo.repositories

import android.content.Context
import android.view.View
import com.nhaarman.mockitokotlin2.mock
import io.bar4ka.demo.core.mvvmp.Presenter
import io.bar4ka.demo.core.navigation.Navigation
import io.bar4ka.demo.main.common.ErrorStateViewModel
import io.bar4ka.demo.main.common.LoadingViewModel
import io.bar4ka.demo.repositories.mappers.RepositoriesMapper
import io.bar4ka.demo.repositories.models.RepositoriesResponse
import io.bar4ka.demo.repositories.viewModels.RepositoriesViewModel
import org.mockito.Mockito.`when`
import rx.Observable

const val USERNAME = "someone"

fun createDependencies(): RepositoriesPresenterDependencies {
    val viewModel = RepositoriesViewModel(ErrorStateViewModel(), LoadingViewModel())
    val service = mock<RepositoriesService>()
    val mapper = mock<RepositoriesMapper>()
    val navigation = mock<Navigation>()
    val view = mock<View>()
    val context = mock<Context>()

    val key = RepositoriesKey(USERNAME)
    `when`(view.context).thenReturn(context)
    `when`(navigation.getKey<RepositoriesKey>(context)).thenReturn(key)

    val presenter = RepositoriesPresenter(
        viewModel,
        service,
        mapper,
        navigation
    )

    return RepositoriesPresenterDependencies(
        presenter,
        service,
        mapper,
        navigation,
        view
    )
}

fun <V> Presenter<V>.setupAttachView(view: V) {
    dispatchCreated()
    dispatchAttachView(view)
}

fun RepositoriesService.setupFetchWillSucceed(params: RepositoriesFetchParams) {
    setupFetchWithObservable(params, Observable.empty())
}

fun RepositoriesService.setupFetchWithObservable(params: RepositoriesFetchParams, observable: Observable<RepositoriesResponse>) {
    `when`(fetch(params)).thenReturn(observable)
}
