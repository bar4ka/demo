package io.bar4ka.demo.repositories

import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import io.bar4ka.demo.main.common.ScreenState
import io.bar4ka.demo.repositories.models.RepositoriesResponse
import org.amshove.kluent.shouldEqual
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import rx.subjects.PublishSubject
import rx.subjects.Subject

object RepositoriesPresenterSpec : Spek({

    given("a RepositoriesPresenter") {

        var dependencies: RepositoriesPresenterDependencies? = null
        var params: RepositoriesFetchParams? = null

        beforeEachTest {
            dependencies = createDependencies()
        }

        context("fetching triggers") {

            beforeEachTest {
                with(dependencies!!) {
                    params = RepositoriesFetchParams(USERNAME, 1, Config.ITEMS_PER_PAGE)
                    service.setupFetchWillSucceed(params!!)
                    presenter.setupAttachView(view)
                }
            }

            on("view is attached") {
                it("should fetch first page") {
                    with(dependencies!!) {
                        verify(service).fetch(eq(params!!))
                    }
                }
            }

            on("try again tapped") {
                it("should fetch first page") {
                    with(dependencies!!) {
                        presenter.viewModel.errorState.onRetry.onNext(null)

                        verify(service, times(2)).fetch(eq(params!!))
                    }
                }
            }

            on("scrolling to 3rd item from the bottom") {
                it("should fetch next page") {
                    with(dependencies!!) {
                    }
                }
            }
        }

        context("loading state") {

            var subject: Subject<RepositoriesResponse, RepositoriesResponse>? = null

            beforeEachTest {
                with(dependencies!!) {
                    params = RepositoriesFetchParams(USERNAME, 1, Config.ITEMS_PER_PAGE)
                    subject = PublishSubject.create()
                    service.setupFetchWithObservable(params!!, subject!!)
                    presenter.setupAttachView(view)
                }
            }

            on("fetching started") {
                it("should display loading state") {
                    with(dependencies!!) {
                        presenter.viewModel.screenState.get() shouldEqual ScreenState.Loading
                    }
                }
            }

            on("fetching emitted items") {
                it("should display content state") {
                    subject!!.onNext(emptyList())

                    with(dependencies!!) {
                        presenter.viewModel.screenState.get() shouldEqual ScreenState.Content
                    }
                }
            }

        }

        context("error state") {

        }

    }

})
