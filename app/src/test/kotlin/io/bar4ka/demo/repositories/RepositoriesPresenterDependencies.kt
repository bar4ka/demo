package io.bar4ka.demo.repositories

import android.view.View
import io.bar4ka.demo.core.navigation.Navigation
import io.bar4ka.demo.repositories.mappers.RepositoriesMapper

data class RepositoriesPresenterDependencies(
    val presenter: RepositoriesPresenter,
    val service: RepositoriesService,
    val mapper: RepositoriesMapper,
    val navigation: Navigation,
    val view: View
)
